import React, {Component, Fragment} from 'react';
import propTypes from 'prop-types';

import ContentsContainer from './ContentsContainer'

export default class Writo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            containersAndParagraphs: []
        }
    }


    render() {
        const {containersAndParagraphs} = this.state;
        return (
            <Fragment>
                <ContentsContainer
                    containersAndParagraphs={containersAndParagraphs}
                />

            </Fragment>
        );
    }
}

