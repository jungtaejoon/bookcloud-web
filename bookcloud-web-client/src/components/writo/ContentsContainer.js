import React, {Component, Fragment} from 'react';
import propTypes from 'prop-types';
import {Col, DropdownButton, Grid, MenuItem, Row, Well} from "react-bootstrap";

import Paragraph from './Paragraph';

import guid from '../../common-function/guid';

import './css/ContentsContainer.css'

export default class ContentsContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            containersAndParagraphs: props.containersAndParagraphs.slice()
        };
    }

    onChange = () => {

    };
    render() {
        const { containersAndParagraphs, title } = this.state;
        return (
            <div>
                <Grid>
                    <Row>
                        <Col xs={8} xsOffset={2}>
                            <div className="card">
                                <Row className="card-head">
                                    <Col sm={9}>
                                        <div contentEditable>{title}</div>
                                    </Col>
                                    <Col sm={3}>
                                        <div className="pull-right">
                                            <DropdownButton
                                                id={`drop-down-${guid()}`}
                                                title="추가"
                                                className=""
                                            >
                                                <MenuItem eventKey="1">박스 추가</MenuItem>
                                                <MenuItem eventKey="2">단락 추가</MenuItem>
                                            </DropdownButton>
                                        </div>

                                    </Col>
                                </Row>
                                <Row className="card-body">
                                    <Col sm={12}>
                                        <Well>
                                           {containersAndParagraphs.map((item, i) => {
                                            return (
                                                item instanceof ContentsContainer &&
                                                <ContentsContainer
                                                    key={i}
                                                    containersAndParagraphs={item.containersAndParagraphs}
                                                /> ||
                                                item instanceof Paragraph &&
                                                <Paragraph
                                                    key={i}
                                                    text={item.text}
                                                    onChange={this.onChange}
                                                />
                                            )
                                        })}</Well>
                                    </Col>
                                </Row>
                            </div>
                        </Col>
                    </Row>

                </Grid>





            </div>
        );
    }
}

ContentsContainer.propTypes = {
    containersAndParagraphs: propTypes.array
};