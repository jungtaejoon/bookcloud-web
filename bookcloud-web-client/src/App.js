import React, {Component, Fragment} from 'react';
import {Route, Switch} from "react-router-dom";
import NavBar from "./components/NavBar";
import Home from "./components/home/Home";
import Writo from "./components/writo/Writo";


import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-theme.min.css';

import './App.css';


class App extends Component {
    render() {
        return (
            <div className="app">
                <NavBar/>
                <div className="container">
                    <Route exact path="/" component={Home}/>

                    <Switch>
                        <Route path="/writo" component={Writo}/>
                    </Switch>

                </div>

            </div>
        );
    }
}

export default App;
